<!--
  ~ Copyright 2016 Webster Smalley
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>uk.co.divisiblebyzero.wordpress</groupId>
    <version>1.2-SNAPSHOT</version>
    <artifactId>wordpress</artifactId>
    <packaging>jar</packaging>
    <name>Java Wordpress client</name>
    <description>Java client for interacting with Wordpress</description>
    <inceptionYear>2016</inceptionYear>
    <url>https://bitbucket.org/dby0/wordpress/overview</url>
    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
            <comments>A business-friendly OSS license</comments>
        </license>
    </licenses>
    <developers>
        <developer>
            <id>dev</id>
            <name>Matthew Smalley</name>
            <email>dev@divisiblebyzero.co.uk</email>
            <url>https://bitbucket.org/dby0</url>
            <organization>Divisible By Zero</organization>
            <organizationUrl>https://bitbucket.org/dby0</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>Europe/London</timezone>
        </developer>
    </developers>
    <scm>
        <connection>scm:git:git@bitbucket.org:dby0/wordpress.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:dby0/wordpress.git</developerConnection>
        <url>https://bitbucket.org/dby0/wordpress/overview</url>
        <tag>HEAD</tag>
    </scm>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <spring.version>4.2.1.RELEASE</spring.version>
        <spring-data.version>1.7.2.RELEASE</spring-data.version>
        <spring-security.version>4.0.3.RELEASE</spring-security.version>
        <log4j.version>1.2.17</log4j.version>
        <jackson-databind.version>2.6.1</jackson-databind.version>
        <jackson-datatype-jsr310.version>2.6.1</jackson-datatype-jsr310.version>
        <junit.version>4.12</junit.version>
        <h2.version>1.4.187</h2.version>
        <hibernate.version>3.6.8.Final</hibernate.version>
        <slf4j.version>1.7.12</slf4j.version>
        <javassist.version>3.12.1.GA</javassist.version>
        <joda-time.version>2.0</joda-time.version>
        <joda-time-hibernate.version>1.3</joda-time-hibernate.version>
        <joda-time-jsptags.version>1.1.1</joda-time-jsptags.version>
        <jetty.version>9.3.2.v20150730</jetty.version>
        <commons-dbcp.version>1.4</commons-dbcp.version>
        <commons-lang.version>3.4</commons-lang.version>
        <mockito.version>1.10.19</mockito.version>
        <commons-beanutils.version>1.9.2</commons-beanutils.version>
        <commons-io.version>2.4</commons-io.version>
        <commons-fileupload.version>1.2.2</commons-fileupload.version>
        <opencsv.version>2.3</opencsv.version>

        <mongo-java-driver.version>3.0.3</mongo-java-driver.version>
        <fongo.version>2.0.1</fongo.version>
        <xmlrpc-client.version>3.1.3</xmlrpc-client.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>com.github.tomakehurst</groupId>
            <artifactId>wiremock</artifactId>
            <version>1.57</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>${log4j.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.xmlrpc</groupId>
            <artifactId>xmlrpc-client</artifactId>
            <version>${xmlrpc-client.version}</version>
        </dependency>
    </dependencies>
    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>
    <profiles>
        <profile>
            <id>ossrh</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-release-plugin</artifactId>
                        <version>2.4.1</version>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.3</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>true</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>2.4</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>2.10.3</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.6</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>

            </build>

        </profile>
    </profiles>
</project>
