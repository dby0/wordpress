# Wordpress Java Client [![Build Status](https://drone.io/bitbucket.org/dby0/wordpress/status.png)](https://drone.io/bitbucket.org/dby0/wordpress/latest) [![Maven Central](https://img.shields.io/maven-central/v/uk.co.divisiblebyzero.wordpress/wordpress.svg)](https://search.maven.org/#artifactdetails%7Cuk.co.divisiblebyzero.wordpress%7Cwordpress%7C1.1%7C)

## Usage

1. Include in your project using Maven or Gradle as appropriate
2. Instantiate a WordPressRpcClient object
3. Set the *required parameters* :
  1 username
  2 password
  3 xmlrpcurl
4. Run the wordpress commands you want to use!

## Currently supported commands

### getPosts

Returns a Map<String, Post> keyed on the name of the post

## getCategories

Returns a Set<String> of categories

## uploadImage

Uploads an image, optionally linking to a post

## addPost

Adds a post, or if the submitted post has an id, will update the post instead


