package uk.co.divisiblebyzero.wordpress;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import org.junit.ClassRule;
import org.junit.Rule;


/**
 * Created by: Matthew Smalley
 * Date: 06/01/2016
 */
public class TestWordPressRpcClient {
    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(8080);

    @Rule
    public WireMockClassRule instanceRule = wireMockRule;

    private WordPressRpcClient wordPressRpcClient;

/*    @BeforeClass
    public static void setupClass() {
        System.setProperty("http.proxyHost", "localhost");
        System.setProperty("http.proxyPort", "8888");
    }

    @Before
    public void setup() {
        wordPressRpcClient = new WordPressRpcClient();
        wordPressRpcClient.setUsername("unituser");
        wordPressRpcClient.setPassword("unitpass");
        wordPressRpcClient.setXmlrpcUrl("http://localhost:8080/xmlrpc.php");

        wireMockRule.stubFor(post(urlEqualTo("/xmlrpc.php")).willReturn(aResponse().withBody("HELLO WORLD")));
    }

    @Test
    public void testCheckLoggedIn() {

        wordPressRpcClient.checkLoggedIn();
    }*/


}
