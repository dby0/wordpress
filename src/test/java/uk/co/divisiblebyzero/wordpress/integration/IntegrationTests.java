package uk.co.divisiblebyzero.wordpress.integration;

import org.junit.Before;
import org.junit.Test;
import uk.co.divisiblebyzero.wordpress.Post;
import uk.co.divisiblebyzero.wordpress.WordPressRpcClient;

import java.util.Map;
import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 06/01/2016
 */
public class IntegrationTests {
    private WordPressRpcClient client;

    @Before
    public void setup() {
        client = new WordPressRpcClient();
        client.setUsername("integrationuser");
        client.setPassword("integrationpass");
        client.setXmlrpcUrl("http://192.168.99.100:8080/xmlrpc.php");
    }

    @Test
    public void test() {
        client.retrievePosts();
    }

    @Test
    public void testAddPost() {
        Set<String> categories = client.getCategories();
        System.out.println(categories);

        Map<String, Post> posts = client.getPosts();
        System.out.println(posts);

        Post p = new Post();
        p.setPostTitle("test post: " + System.currentTimeMillis());
        p.getCategories().add("Introductions");
        System.out.println(p);
        client.addPost(p);
        System.out.println(p);

        System.out.println(client.getPosts());

        p.setPostTitle("edited test post: " + System.currentTimeMillis());
        p.getCategories().add("Updated Category");
        client.addPost(p);
        System.out.println(p);
        System.out.println(client.getPosts());

    }
}
